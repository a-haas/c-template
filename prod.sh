#!/bin/sh

APP_NAME="prod:my-app-c"
PATH_IN_LOCAL_ENV=/home/ahaas/workspace/c-template

docker build -t $APP_NAME . --file $PATH_IN_LOCAL_ENV/prod.Dockerfile
docker run -it -P $APP_NAME