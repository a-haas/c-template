#!/bin/sh

APP_NAME="dev:my-app-c"
PATH_IN_LOCAL_ENV=/home/ahaas/workspace/c-template

docker build -t $APP_NAME . --file $PATH_IN_LOCAL_ENV/dev.Dockerfile
docker run -it -P -v $PATH_IN_LOCAL_ENV:/app $APP_NAME